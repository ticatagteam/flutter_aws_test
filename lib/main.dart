import 'dart:html' as html;
import 'dart:html';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

final requestHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json; charset=UTF-8',
};

void main() => runApp(MyApp());

const String _redirectUri = "https://tifizconnect.codemagic.app/callback.html";
//const String _redirectUri="http://localhost/callback.html";

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: "Home page"));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isConnected = false;
  String _accessToken;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _body(),
    );
  }

  _body() {
    final authorizeParameters = {
      'response_type': 'id_token token',
      'redirect_uri': _redirectUri,
      'client_id': 'tibe-connect-web',
      'scope': '',
      'state': null,
      'nonce': null,
    };
    var uri = Uri.parse('https://account.ticatag.com/uaa/oauth/authorize')
        .replace(queryParameters: authorizeParameters);

    return Column(children: [
      if (_accessToken == null)
        MaterialButton(
            color: _isConnected ? Colors.blue : Colors.red,
            child: Text("popup with token: $_accessToken "),
            onPressed: () {
              final popup = html.window
                  .open(uri.toString(), 'popup', "width=350,height=250");
              html.window.addEventListener('message', (event) {
                final messageEvent = event as MessageEvent;
                final map =
                    Uri.splitQueryString(messageEvent.data.substring(1));
                _accessToken = map['access_token'];
                popup.close();
                setState(() {
                  _isConnected = true;
                });
              });
            }),
      if (_accessToken != null)
        FutureBuilder(
            future: _devices(_accessToken),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text("Data returned: ${snapshot.data}");
              } else {
                return Text("Loading");
              }
            })
    ]);
  }

  _devices(accessToken) async {
    const apiGate = 'https://api.ticatag.com/v2';
    Dio dio = Dio();
    dio.options.baseUrl = apiGate;
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      options.headers['Authorization'] = 'Bearer $accessToken';
      return options;
    }, onResponse: (Response response) {
      return response;
    }));

    final queryParameters = {
      'type': 'ibeacon',
      'includes':
          'name,status,device_id,last_location,ibeacon_identifiers,product,images,last_battery,mac_address '
    };

    final response =
        await dio.get('/devices', queryParameters: queryParameters);
    if (response.data['_embedded'] != null &&
        response.data['_embedded']['devices'] != null) {
      final devices = response.data['_embedded']['devices'];
      print(devices);
    }
    return response;
  }
}
