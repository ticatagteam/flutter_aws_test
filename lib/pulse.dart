import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PulseController {
  final _PulseState pulse;
  PulseController(this.pulse);

  setPercent(double value) {
    pulse.updatePercent(value);
  }
}

typedef OnPulseCreated(PulseController controller);

class Pulse extends StatefulWidget {
  final Widget child;
  final Color color;
  final Color backgroundColor;
  final Color borderColor;
  final double percent;
  final double size;
  final double childSize;
  OnPulseCreated onPulseCreated;

  Pulse(
      {this.child,
      this.size = 400,
      this.childSize = 50,
      this.color = Colors.blue,
      this.backgroundColor = Colors.white,
      this.borderColor = Colors.grey,
      this.percent = 0,
      this.onPulseCreated});

  @override
  _PulseState createState() => _PulseState();
}

class _PulseState extends State<Pulse> {
  PulseController pulseController;
  List<PulseElement> elements = [];
  double _percent;
  Timer _timer;

  @override
  void initState() {
    _percent = widget.percent;
    _timer = Timer.periodic(Duration(milliseconds: 500), (_) {
      setState(() {
        final computedSize =
            widget.childSize + (widget.size - widget.childSize) * _percent;
        elements.add(PulseElement(
          Key(DateTime.now().toIso8601String()),
          color: widget.color,
          initialSize: 50,
          endSize: computedSize,
          onPulseCompleted: (pulse) {
            elements.remove(pulse);
          },
        ));
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  updatePercent(double value) {
    //elements.clear();
    _percent = value;
  }

  @override
  void didChangeDependencies() {
    pulseController = PulseController(this);
    if (widget.onPulseCreated != null) {
      widget.onPulseCreated(pulseController);
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final computedSize =
        widget.childSize + (widget.size - widget.childSize) * _percent;
    return Stack(
      alignment: Alignment.center,
      children: [
        Align(
          alignment: Alignment.center,
          child: Container(
            width: widget.size,
            height: widget.size,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: widget.backgroundColor,
                border: Border.all(color: widget.borderColor)),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: ClipOval(
              child: Container(
                  width: computedSize,
                  height: computedSize,
                  color: widget.color.withOpacity(0.3))),
        ),
      ],
    );
  }
}

typedef OnPulseCompleted(PulseElement element);

class PulseElement extends StatefulWidget {
  final OnPulseCompleted onPulseCompleted;
  final Color color;
  final double initialSize;
  final double endSize;

  PulseElement(Key key,
      {this.onPulseCompleted,
      this.color = Colors.blue,
      this.initialSize = 0,
      @required this.endSize})
      : super(key: key);

  @override
  _PulseElementState createState() => _PulseElementState();
}

class _PulseElementState extends State<PulseElement> {
  double _size;
  Color _color;

  @override
  void initState() {
    _size = widget.initialSize;
    _color = widget.color;

    Timer(Duration(milliseconds: 10), () {
      setState(() {
        _color = _color.withOpacity(0);
        _size = widget.endSize;
      });
    });

    super.initState();
  }

  cancel() {
    setState(() {
      _size = widget.initialSize;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: ClipOval(
          child: AnimatedContainer(
              onEnd: () {
                if (widget.onPulseCompleted != null)
                  widget.onPulseCompleted(widget);
              },
              width: _size,
              height: _size,
              color: _color,
              curve: Curves.ease,
              duration: Duration(milliseconds: 5000))),
    );
  }
}
