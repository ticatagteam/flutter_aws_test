import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

Future<void> _takeScreenshot(FlutterDriver driver, String path) async {
  final List<int> pixels = await driver.screenshot();
  final File file = new File(path);
  await file.writeAsBytes(pixels);
  print(path);
}

void main() {
  group('end-to-end test', () {
    final counterTextFinder = find.byValueKey('counter');
    FlutterDriver driver;

    setUpAll(() async {
      // Connect to a running Flutter application instance.
      //Directory('screenshots').create();
      driver = await FlutterDriver.connect();
      await Future.delayed(Duration(seconds: 1));
    });

    tearDownAll(() async {
      if (driver != null) await driver.close();
    });

    test('tap on the drawer button to open settings', () async {
      await _takeScreenshot(driver, 'home-0.png');
      SerializableFinder floatingFinder = find.byTooltip('Increment');
      await driver.tap(floatingFinder);
      await _takeScreenshot(driver, 'home-1.png');
      //await Future.delayed(Duration(seconds: 5));
      expect(await driver.getText(counterTextFinder), "1");
    });
  });
}
