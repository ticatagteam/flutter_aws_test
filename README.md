# flutter_aws_test

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Local tests:
flutter drive --target=test_driver/main.dart

AWS tests:
export TEAM_ID=XVAN3T9289
sylph -c lib/sylph.yalm



https://pub.dev/packages/dhttpd
dhttpd -p 3000 --path build/web/


